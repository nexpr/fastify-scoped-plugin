import Fastify from "fastify"
import manual from "./manual/app.js"
import nested_autoload from "./nested-autoload/app.js"
import autohooks from "./autohooks/app.js"

const run = async (app) => {
	const fastify = Fastify()
	await fastify.register(app)

	console.log(fastify.printRoutes())
	console.log(fastify.printPlugins())

	await fastify.inject({ method: "GET", url: "/foo/bar/baz/route1" })
	await fastify.inject({ method: "GET", url: "/foo/bar/route2" })
	await fastify.inject({ method: "GET", url: "/foo/bar/route3" })
	await fastify.inject({ method: "GET", url: "/foo/route4" })
	await fastify.inject({ method: "GET", url: "/route5" })
}

console.log("\n==== manual ====\n")

await run(manual)

console.log("\n==== nested autoload ====\n")

await run(nested_autoload)

console.log("\n==== autohooks ====\n")

await run(autohooks)
