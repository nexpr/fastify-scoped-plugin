import autoload from "./autoload.js"

// Pass --options via CLI arguments in command to enable these options.
export const options = {}

export default async function (fastify, opts) {
	autoload(fastify, import.meta.url, opts)
}
