import path from "path"
import AutoLoad from "@fastify/autoload"
import { fileURLToPath } from "url"

const toDirName = (url) => {
	const __filename = fileURLToPath(url)
	const __dirname = path.dirname(__filename)
	return __dirname
}

export default (fastify, base_url, options) => {
	const dirname = toDirName(base_url)
	const { prefix: _, ...opts } = options

	// This loads all plugins defined in plugins
	// those should be support plugins that are reused
	// through your application
	fastify.register(AutoLoad, {
		dir: path.join(dirname, "plugins"),
		options: Object.assign({}, opts)
	})

	// This loads all plugins defined in routes
	// define your routes in one of these
	fastify.register(AutoLoad, {
		dir: path.join(dirname, "routes"),
		options: Object.assign({}, opts),
		ignorePattern: /^(routes|plugins)$/
	})
}