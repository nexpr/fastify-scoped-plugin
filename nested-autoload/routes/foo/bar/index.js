import autoload from "../../../autoload.js"

export default async function (fastify, opts) {
	autoload(fastify, import.meta.url, opts)
}
