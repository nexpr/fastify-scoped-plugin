import fp from "fastify-plugin"

export default async (fastify, opts) => {
	await fastify.register(fp(async (fastify, opts) => {
		fastify.decorate("value1", "VALUE")
	}))

	await fastify.register(async (fastify, opts) => {
		await fastify.register(async (fastify, opts) => {
			await fastify.register(fp(async (fastify, opts) => {
				fastify.decorate("value2", "VALUE")
			}))

			await fastify.register(async (fastify, opts) => {
				fastify.get("/route1", async (request, reply) => {
					console.log("route1", fastify.value1, fastify.value2) // VALUE VALUE
					// ...
				})
			}, { prefix: "/baz", name: "baz" })

			fastify.get("/route2", async (request, reply) => {
				console.log("route2", fastify.value1, fastify.value2) // VALUE VALUE
				// ...
			})

			fastify.get("/route3", async (request, reply) => {
				console.log("route3", fastify.value1, fastify.value2) // VALUE VALUE
				// ...
			})
		}, { prefix: "/bar", name: "bar" })

		fastify.get("/route4", async (request, reply) => {
			console.log("route4", fastify.value1, fastify.value2) // VALUE undefined
			// ...
		})
	}, { prefix: "/foo", name: "foo" })

	fastify.get("/route5", async (request, reply) => {
		console.log("route5", fastify.value1, fastify.value2) // VALUE undefined
		// ...
	})
}

